#include "oauth2_session.h"

//
// Specialized class for Eve Online OAuth 2.0 session.
//
class eve_oauth2_session : public oauth2_session
{
public:
	eve_oauth2_session() :
		oauth2_session(U("EveCREST"),
		U(CLIENT_KEY),
		U(CLIENT_SECRET),
		U("https://sisilogin.testeveonline.com/oauth/authorize"),
		U("https://sisilogin.testeveonline.com/oauth/token"),
		U("http://localhost:8889/"))
	{
		m_oauth2_config.set_scope(U("publicData characterStatisticsRead"));
	}

protected:
	void run_internal() override
	{
		http_client api(U("https://api-sisi.testeveonline.com"), m_http_config);
		ucout << "Requesting CREST information:" << std::endl;

		http_request request(methods::OPTIONS);
		//http_request request(methods::GET);
		request.set_request_uri(uri(U("/decode/")));
		request.headers().add(header_names::accept, "application/vnd.ccp.eve.Api-v3+json");
		auto response = api.request(request).get();
		auto body = response.extract_json(true).get();
		ucout << "Information: " << body << std::endl;
	}
};
