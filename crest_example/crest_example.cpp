#include "tchar.h"

#include "eve_oauth2_session.h"

int _tmain(int argc, _TCHAR* argv[])
{
	eve_oauth2_session session;

	session.run();

	return 0;
}

