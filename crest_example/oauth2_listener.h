#include <mutex>

#include "cpprest/http_listener.h"
#include "cpprest/oauth2.h"

using namespace utility;
using namespace web;
using namespace web::http;
using namespace web::http::oauth2::experimental;
using namespace web::http::experimental::listener;

//
// A simple listener class to capture OAuth 2.0 HTTP redirect to localhost.
// The listener captures redirected URI and obtains the token.
// This type of listener can be implemented in the back-end to capture and store tokens.
//
class oauth2_listener
{
public:
	oauth2_listener(
		uri listen_uri,
		oauth2_config& config) :
		m_listener(new http_listener(listen_uri)),
		m_config(config)
	{
		m_listener->support([this](http::http_request request) -> void
		{
			if (request.request_uri().path() == U("/") && request.request_uri().query() != U(""))
			{
				m_resplock.lock();

				m_config.token_from_redirected_uri(request.request_uri()).then([this, request](pplx::task<void> token_task) -> void
				{
					try
					{
						token_task.wait();
						m_tce.set(true);
					}
					catch (const oauth2_exception& e)
					{
						ucout << "Error: " << e.what() << std::endl;
						m_tce.set(false);
					}
				});

				request.reply(status_codes::OK, U("Ok."));

				m_resplock.unlock();
			}
			else
			{
				request.reply(status_codes::NotFound, U("Not found."));
			}
		});

		m_listener->open().wait();
	}

	~oauth2_listener()
	{
		m_listener->close().wait();
	}

	pplx::task<bool> listen_for_code()
	{
		return pplx::create_task(m_tce);
	}

private:
	std::unique_ptr<http_listener> m_listener;
	pplx::task_completion_event<bool> m_tce;
	oauth2_config& m_config;
	std::mutex m_resplock;
};